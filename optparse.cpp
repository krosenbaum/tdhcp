/*
*  C Implementation: option parser
*
* Description: a convenient C++ wrapper for GNU getopt
*
*
* Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2012
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include "optparse.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <functional> 
#include <locale>


using namespace std;
using namespace OptParse;

const Option Option::ArgumentOptional=NeedArg::OptArg;
const Option Option::ArgumentRequired=NeedArg::ReqArg;

Argument::Argument(const Option& o, OptionCallback oc)
	:m_call(oc)
{
	setOption(o);
}

Argument::Argument(const std::initializer_list< Option >& ol, OptionCallback oc)
	:m_call(oc)
{
	for(const Option&o:ol)setOption(o);
}

bool Argument::matches(const std::string& s) const
{
	for(const auto o:m_opt)
		if(o==s)return true;
	return false;
}

bool Argument::matches(char c) const
{
	string s;
	s+=c;
	return matches(s);
}

void Argument::setOption(const Option& o)
{
	if(o.type()==Option::Type::OptName)
		m_opt.push_back(o.name());
	if(o.type()==Option::Type::NeedArg)
		if(o.needArg()!=Option::NeedArg::Null)
			m_arg=o.needArg();
	if(o.type()==Option::Type::Description)
		m_descr=o.name();
}

Argument Parser::get(const std::string& n) const
{
	for(auto a:m_args)
		if(a.matches(n))
			return a;
	return Argument();
}

void Parser::remove(const std::string& n)
{
	remove_if(m_args.begin(),m_args.end(),[=](const Argument&a)->bool{return a.matches(n);});
}

void Parser::setOptionCallback(const std::string& n, OptionCallback cb)
{
	for(auto a:m_args)if(a.matches(n))a.setCallback(cb);
}

void Parser::parseArguments(int argc, const char *const* argv,bool ig) const
{
	std::list<std::string>args;
	for(int i=ig?1:0;i<argc;i++)args.push_back(std::string(argv[i]));
	parseArguments(args,false);
}

void Parser::parseArguments(std::list< std::string > argv,bool ig) const
{
	if(ig)argv.pop_front();
	for(auto arg:argv)parseArgument(arg,true);
}

static inline string trim(std::string s) {
	static auto pred=[](char c){return !std::isspace(c);};
	s.erase(s.begin(),find_if(s.begin(),s.end(),pred));
	s.erase(find_if(s.rbegin(),s.rend(),pred).base(),s.end());
        return s;
}

void Parser::parseArgument(const std::string& arg,bool useprefix)const
{
	const auto prfs=useprefix?m_prefix.size():0;
	//is it an option style argument?
	if(!useprefix || m_prefix.empty() || arg.substr(0,prfs)==m_prefix){
		//parse it
		std::string name,val;
		auto pe=arg.find('=',prfs);
		bool haveArg=false;
		if(pe==arg.npos)
			name=trim(arg.substr(prfs));
		else{
			name=trim(arg.substr(prfs,pe-prfs));
			val=trim(arg.substr(pe+1));
			haveArg=true;
		}
		//find option
		bool found=false;
		for(auto argi:m_args){
			if(argi.matches(name)){
				found=true;
				if(argi.needArg()==Option::NeedArg::ReqArg&&!haveArg){
					if(m_ecall)m_ecall(arg,Error::MissingArgument);
					return;
				}
				if(argi.needArg()==Option::NeedArg::NoArg&&haveArg){
					if(m_ecall)m_ecall(arg,Error::UnexpectedArgument);
					return;
				}
				argi(name,val);
				break;
			}
		}
		if(found){
			if(m_call)m_call(name,val);
		}else{
			if(m_ecall)m_ecall(arg,Error::UnknownOption);
		}
	}else{
		//raise as non-option
		if(m_ncall)m_ncall(arg);
	}
}

void Parser::parseFile(const std::string& filename) const
{
	ifstream in(filename);
	parseFile(in);
}

void Parser::parseFile(std::istream& stream) const
{
	//get lines
	while(stream.good() && !stream.eof()){
		string line;
		getline(stream,line);
		line=trim(line);
		//check whether line empty
		if(line.empty())continue;
		//is it a comment
		if(line[0]=='#')continue;
		//normal parsing
		parseArgument(line,false);
	}
}

void Parser::printHelp(int base, int indent) const
{
	//calc indents
	std::string basestr,indstr;
	for(int i=0;i<base;i++)basestr+=' ';
	for(int i=0;i<(base+indent);i++)indstr+=' ';
	//print
	for(auto arg:m_args){
		cout<<basestr;
		bool na=false,nac=false;
		switch(arg.needArg()){
			case Option::NeedArg::OptArg:nac=true;//fallthrough
			case Option::NeedArg::ReqArg:na=true;break;
			default:break;
		}
		bool nfirst=false;
		for(auto opt:arg.options()){
			if(nfirst)cout<<" | ";
			nfirst=true;
			cout<<m_prefix<<opt;
			if(nac)cout<<"[";
			if(na)cout<<"=arg";
			if(nac)cout<<"]";
		}
		cout<<"\n"<<indstr<<arg.description()<<"\n";
	}
}

