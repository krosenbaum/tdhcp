// C Interface: getopt
//
// Description: a convenient C++ wrapper for GNU getopt
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef OPTPARSE_H
#define OPTPARSE_H

#include <vector>
#include <string>
#include <functional>
#include <initializer_list>
#include <list>
#include "optparse.h"

namespace OptParse {

class Option
{
	public:
		enum class Type {Null=0,OptName,NeedArg,Description};
		enum class NeedArg{Null=0,NoArg,OptArg,ReqArg};
		
		Option():m_type(Type::Null),m_arg(NeedArg::Null){}
		Option(char s):m_type(Type::OptName),m_arg(NeedArg::Null){m_str+=s;}
		Option(std::string s,Type t=Type::OptName):m_type(t),m_str(s),m_arg(NeedArg::Null){}
		Option(NeedArg a):m_type(Type::NeedArg),m_arg(a){}
		Option(const Option&o)=default;
		Option(Option&&o)=default;
		Option& operator=(const Option&)=default;
		Option& operator=(Option&&)=default;
		
		Type type()const{return m_type;}
		NeedArg needArg()const{return m_arg;}
		std::string name()const{return m_str;}
		
		static const Option ArgumentOptional;
		static const Option ArgumentRequired;

	private:
		Type m_type;
		std::string m_str;
		NeedArg m_arg;
};

inline Option operator"" _Opt(char const* s,size_t sz){return Option(std::string(s,sz));}
inline Option operator"" _Opt(char s){std::string st;st+=s;return Option(st);}
inline Option operator"" _Descr(char const* s,size_t sz){return Option(std::string(s,sz),Option::Type::Description);}

typedef std::function<void(std::string,std::string)>OptionCallback;

enum class Error{Ok=0,UnknownOption,MissingArgument,UnexpectedArgument};
typedef std::function<void(std::string,Error)>ErrorCallback;

inline std::string error2String(Error e)
{
	switch(e){
		case Error::Ok:return "No Error";
		case Error::UnknownOption:return "unkown option";
		case Error::MissingArgument:return "argument expected";
		case Error::UnexpectedArgument:return "no argument expected";
		default:return "unknown error";
	}
}

typedef std::function<void(std::string)>NonOptionCallback;

class Argument
{
	std::vector<std::string> m_opt;
	Option::NeedArg m_arg;
	OptionCallback m_call;
	std::string m_descr;
	public:
		Argument():m_arg(Option::NeedArg::Null){}
		Argument(const Option&o,OptionCallback oc=nullptr);
		Argument(const std::initializer_list<Option>&,OptionCallback oc=nullptr);
		Argument(const Argument&)=default;
		Argument(Argument&&)=default;
		
		Argument& operator=(const Argument&)=default;
		Argument& operator=(Argument&&)=default;
		
		Option::NeedArg needArg()const{return m_arg;}
		OptionCallback callback()const{return m_call;}
		
		bool matches(const std::string&s)const;
		bool matches(char)const;
		
		void setOption(const Option&);
		void setCallback(OptionCallback oc){m_call=oc;}
		
		void setDescription(const std::string&s){m_descr=s;}
		std::string description()const{return m_descr;}
		
		std::vector<std::string> options()const{return m_opt;}
		
		void operator()(std::string name,std::string value)const
		{if(m_call)m_call(name,value);}
};

class Parser
{
	std::vector<Argument>m_args;
	OptionCallback m_call;
	ErrorCallback m_ecall;
	NonOptionCallback m_ncall;
	std::string m_prefix="-";
	public:
		Parser(){}
		Parser(const Parser&)=default;
		Parser(Parser&&)=default;
		Parser(const std::initializer_list<Argument>&a):m_args(a){}
		
		void add(const Argument&a){m_args.push_back(a);}
		void add(const std::initializer_list<Argument>&al)
		{for(auto a:al)m_args.push_back(a);}
		
		void remove(const std::string&);
		
		void setOptionCallback(const std::string&,OptionCallback);
		Argument get(const std::string&)const;
		
		void setGenericOptionCallback(OptionCallback cb){m_call=cb;}
		void setErrorCallback(ErrorCallback ec){m_ecall=ec;}
		void setNonOptionCallback(NonOptionCallback nc){m_ncall=nc;}
		
		void parseArguments(int argc,const char *const*argv,bool ignoreFirst=true)const;
		void parseArguments(std::list<std::string>argv,bool ignoreFirst=false)const;
		
		void parseFile(const std::string &filename)const;
		void parseFile(std::istream &stream)const;
		
		void printHelp(int base=0,int indent=2)const;
	private:
		void parseArgument(const std::string&,bool)const;
};

};//End of namespace
#endif
